﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Example.Model;
using Newtonsoft.Json;

namespace Example.WebApi.Client
{
    public class WebApiClient
    {
        HttpClient _client = new HttpClient();
        public WebApiClient()
        {
            _client.BaseAddress = new Uri("http://localhost:9115");
        }
        
        public IEnumerable<Recipe> GetRecipes()
        {
           
            

            var response = _client.GetAsync("recipes").Result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var stringResult = response.Content.ReadAsStringAsync().Result;
                var obj = JsonConvert.DeserializeObject<List <Recipe>>(stringResult);
                return obj;
            }
            else
            {
                throw new Exception("Wrong response code");
            }
            

        }

        public void CreateRecipe(Recipe resultRecipe)
        {
            var stringJson = JsonConvert.SerializeObject(resultRecipe);
            HttpContent content = new StringContent(stringJson,Encoding.UTF8,"application/json");
            var response = _client.PostAsync("recipes", content).Result;
            
        }

        public IEnumerable<Recipe> FindRecipeByName(string searchPattern)
        {
            var response = _client.GetAsync($"recipes?q={searchPattern}").Result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var stringResult = response.Content.ReadAsStringAsync().Result;
                var obj = JsonConvert.DeserializeObject<List<Recipe>>(stringResult);
                return obj;
            }
            else
            {
                throw new Exception("Wrong response code");
            }


        }

        public Recipe GetRecipe(int selectedRecipe)
        {
            var response = _client.GetAsync($"recipes/{selectedRecipe}").Result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var stringResult = response.Content.ReadAsStringAsync().Result;
                var obj = JsonConvert.DeserializeObject<Recipe>(stringResult);
                return obj;
            }
            else
            {
                throw new Exception("Wrong response code");
            }

        }

        public void UpdateRecipe(int selectedRecipe,Recipe recipe)
        {
            var stringJson = JsonConvert.SerializeObject(recipe);
            HttpContent content = new StringContent(stringJson, Encoding.UTF8, "application/json");
            var response = _client.PutAsync($"recipes/{selectedRecipe}",content).Result;
        }

        public IEnumerable<Ingredient> GetIngredients(int selectedRecipe)
        {
            var response = _client.GetAsync($"recipes/{selectedRecipe}/ingredients").Result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var stringResult = response.Content.ReadAsStringAsync().Result;
                var obj = JsonConvert.DeserializeObject<List<Ingredient>>(stringResult);
                return obj;
            }
            else
            {
                throw new Exception("Wrong response code");
            }

        }

        public void DeleteRecipe(int selectedRecipe)
        {
            var response = _client.DeleteAsync($"recipes/{selectedRecipe}").Result;

        }
    }

   
}
