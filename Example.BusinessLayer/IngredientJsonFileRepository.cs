﻿using Example.Model;

namespace Example.BusinessLayer
{
    public class IngredientJsonFileRepository : GenericJsonFileRepository<Ingredient>,IIngredientRepository
    {
        public IngredientJsonFileRepository() : base("ingredients.json")
        {

        }

        public void AddOrUpdate(Ingredient ingredient)
        {
            Add(ingredient);
        }
    }
}