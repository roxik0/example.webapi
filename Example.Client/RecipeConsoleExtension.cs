﻿using System;
using System.Linq;
using Example.Model;

namespace Example.Client
{
    static class RecipeConsoleExtension
    {
        public static void ShowOnConsole(this Recipe recipe)
        {
            Console.WriteLine(new string('#', Console.BufferWidth));
            Console.WriteLine($"Title: {recipe.Title}");
            Console.WriteLine($"Ingredients:");
            Console.WriteLine($"{string.Join(Environment.NewLine, recipe.Ingredients.Select(c => $"{c.Name} {c.Amount}"))}");


        }

      
    }
}