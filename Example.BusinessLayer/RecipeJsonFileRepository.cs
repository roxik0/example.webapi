﻿using System.Collections.Generic;
using System.Linq;
using Example.Model;

namespace Example.BusinessLayer
{
    public class RecipeJsonFileRepository : GenericJsonFileRepository<Recipe>, IRecipeRepository
    {
        private readonly IIngredientRepository _ingredientRepository;

        public RecipeJsonFileRepository(IIngredientRepository ingredientRepository):base("Recipe.json")
        {
            _ingredientRepository = ingredientRepository;
        }

        public void CreateRecipe(Recipe recipe)
        {
            Add(recipe);
            foreach (var ingredient in recipe.Ingredients)
            {
                _ingredientRepository.AddOrUpdate(ingredient);
            }
        }

        public IEnumerable<Recipe> GetAll()
        {
            return base.GetAll();
        }

        public void Update(int id, Recipe recipe)
        {
            base.Update(id, recipe);
        }

        public void Delete(int id)
        {
            base.Delete(id);
        }

        public Recipe Get(int id)
        {
            return base.GetAll().ToList()[id];
        }
    }
}