﻿using System;

namespace Example.Client
{
    class MenuItem
    {
        public string Title { get; set; }
        public Action Action { get; set; }
    }
}