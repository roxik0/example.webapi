﻿using Example.Model;

namespace Example.BusinessLayer
{
    public interface IIngredientRepository
    {
        void AddOrUpdate(Ingredient ingredient);
    }
}