﻿using System;
using System.Collections.Generic;
using System.Linq;
using Example.Model;
using Example.WebApi.Client;

namespace Example.Client
{
    class MainMenu:Menu
    {
        public MainMenu()
        {
            Title = "Main Menu";
            InitializeMenu();
        }

        private void InitializeMenu()
        {
            AddMenuItem("Show Recipes", ShowRecipes);
            AddMenuItem("Find Recipes by Name", FindRecipesByName);

            AddMenuItem("Create Recipe", CreateRecipe);
            AddMenuItem("Exit",ExitApp);

            
        }

        private void FindRecipesByName()
        {
            string currentSearch = "";
            IEnumerable<Recipe> recipes = new List<Recipe>();
            while (true)
            {
                Console.Clear();
                foreach (var rec in recipes)
                {
                    Console.WriteLine(rec.Title);
                }
                Console.Write($"Search: {currentSearch}");
                var key=Console.ReadKey();
                if (key.Key == ConsoleKey.Enter)
                {
                    break;
                }else if (key.Key == ConsoleKey.Backspace)
                {
                    if (currentSearch.Length > 1)
                    {
                        currentSearch = currentSearch.Substring(0, currentSearch.Length - 1);
                    }
                    else
                    {
                        currentSearch = "";
                    }
                }
                else { currentSearch += key.KeyChar; }

                recipes = new WebApiClient().FindRecipeByName(currentSearch);
                

            }
            var recipe=new WebApiClient().FindRecipeByName(currentSearch).FirstOrDefault();
            recipe.ShowOnConsole();
            
        }

       
        private void CreateRecipe()
        {
            Console.Clear();
            var createRecipeMenu = new RecipeMenu();
            createRecipeMenu.Run();
            new WebApiClient().CreateRecipe(createRecipeMenu.ResultRecipe);


        }

        private void ExitApp()
        {
            ExitMenu = true;

        }

        private void ShowRecipes()
        {
            var apiClient = new WebApiClient();
            var recipes=apiClient.GetRecipes().ToList();
            int index = 0;
            foreach (var recipe in recipes)
            {
                index++;
                Console.WriteLine($"{index}. {recipe.Title}");
            }
            Console.Write("Select recipe: ");
            var line=Console.ReadLine();
            int choosen = 0;
            if (int.TryParse(line, out choosen))
            {
                
                var selectedRecipeMenu = new SelectedRecipeMenu(choosen-1){Title = recipes[choosen - 1].Title };
                selectedRecipeMenu.Run();
            }
             

        }

       
    }
}