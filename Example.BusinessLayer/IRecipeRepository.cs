﻿using System.Collections.Generic;
using Example.Model;

namespace Example.BusinessLayer
{
    public interface IRecipeRepository
    {
        void CreateRecipe(Recipe recipe);
        IEnumerable<Recipe> GetAll();
        void Update(int id, Recipe recipe);
        void Delete(int id);
        Recipe Get(int id);
    }
}