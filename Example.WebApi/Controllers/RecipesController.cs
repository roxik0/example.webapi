﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Example.BusinessLayer;
using Example.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Example.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RecipesController : ControllerBase
    {
        private IRecipeRepository recipeRepository;
        public RecipesController()
        {
            recipeRepository = new RecipeJsonFileRepository(new IngredientJsonFileRepository());
        }


        [HttpGet]
        public IEnumerable<Recipe> Get([FromQuery] string q = "")
        {
            return recipeRepository.GetAll().Where(p => p.Title.StartsWith(q));
        }
        [HttpGet("{id}")]
        public Recipe Get(int id)
        {
            return recipeRepository.Get(id);
        }
        [HttpGet("{id}/ingredients")]
        public IEnumerable<Ingredient> GetIngredients(int id)
        {
            return recipeRepository.Get(id).Ingredients;
        }
        [HttpPost]
        public void Post([FromBody] Recipe recipe)
        {
            recipeRepository.CreateRecipe(recipe);
        }
        [HttpPut("{id}")]
        public void Post([FromBody] Recipe recipe, int id)
        {
            recipeRepository.Update(id, recipe);
        }
        [HttpDelete("{id}")]
        public void Delete( int id)
        {
            recipeRepository.Delete(id);
        }

    }
}
