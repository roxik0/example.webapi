﻿using System;
using System.Collections.Generic;

namespace Example.Client
{
    class Menu
    {
        public string Title { get; set; }
        public bool ExitMenu { get; set; }
        private List<MenuItem> _menuItems = new List<MenuItem>();

        protected void AddMenuItem(string title, Action action)
        {
            _menuItems.Add(new MenuItem(){Title = title,Action = action});
        }
        public void Run()
        {
                while (!ExitMenu)
                {
                    Console.Clear();
                    Console.WriteLine(new string('#',Console.BufferWidth-1));
                    Console.WriteLine(Title);
                    Console.WriteLine(new string('#', Console.BufferWidth-1));
                    foreach (var menuItem in _menuItems)
                    {
                        Console.WriteLine($"{_menuItems.IndexOf(menuItem) + 1}.{menuItem.Title}");
                    }

                    Console.WriteLine("Choose menu number");
                    var line = Console.ReadLine();
                    int choosen = 0;
                    if (int.TryParse(line, out choosen))
                    {
                        _menuItems[choosen - 1].Action?.Invoke();
                    }
                }
            
        }

        protected void ShowPressAnyKeyToContinue()
        {
            Console.Write("Press any key to continue");
            Console.ReadKey();
        }
    }
}