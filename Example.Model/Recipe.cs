﻿using System;
using System.Collections.Generic;

namespace Example.Model
{
    public class Recipe
    {
        public string Title { get; set; }
        public List<Ingredient> Ingredients { get; set; } = new List<Ingredient>();

    }
}
