﻿using System;
using System.Linq;
using Example.Model;

namespace Example.Client
{
    internal class RecipeMenu:Menu
    {
        public Recipe ResultRecipe { get; set; } = new Recipe();
        public RecipeMenu()
        {
            AddMenuItem("Set Title", SetTitle);
            AddMenuItem("Show Ingredients", ShowIngredients);
            AddMenuItem("Add Ingredients", AddIngredients);
            AddMenuItem("End",EndOfCreation);
        }

        private void ShowIngredients()
        {
            Console.Clear();
            Console.WriteLine($"Current Ingredients: {ResultRecipe.Ingredients.Select(c => $"{c.Name} {c.Amount}")}");
            ShowPressAnyKeyToContinue();
        }

        private void AddIngredients()
        {
            ShowIngredients();
            Ingredient ingredient=new Ingredient();
            Console.Write("Name of Ingredient: ");
            var name=Console.ReadLine();
            ingredient.Name = name;
            Console.Write("Amount of Ingredient: ");
            var amount = Console.ReadLine();
            ingredient.Amount = amount;

            ResultRecipe.Ingredients.Add(ingredient);

        }

        private void EndOfCreation()
        {
            ExitMenu = true;
        }

        private void SetTitle()
        {
            Console.Clear();
            Console.WriteLine($"Current Title: {ResultRecipe.Title}");
            Console.Write("New Value:");
            var title=Console.ReadLine();
            ResultRecipe.Title = title;
            Title = title;
        }

        
    }
}