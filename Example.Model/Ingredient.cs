﻿namespace Example.Model
{
    public class Ingredient
    {
        public string Name { get; set; }
        public string Amount { get; set; }
    }
}