﻿using System.Reflection;

namespace Example.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var mainMenu = new MainMenu();
            mainMenu.Run();

        }
    }
}
