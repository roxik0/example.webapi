﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace Example.BusinessLayer
{
    public abstract class GenericJsonFileRepository<T>
    {
        private List<T> objects=new List<T>();
        private readonly string _fileName;

        protected GenericJsonFileRepository(string fileName)
        {
            _fileName = fileName;
        }

        protected void Add(T obj)
        {
            LoadFile();
            objects.Add(obj);
            SaveFile();
        }

        private void SaveFile()
        {
            var txt=JsonConvert.SerializeObject(objects);
            File.WriteAllText(_fileName,txt);
        }

        private void LoadFile()
        {
            if (File.Exists(_fileName))
            {
                var txt = File.ReadAllText(_fileName);
                var list = JsonConvert.DeserializeObject<List<T>>(txt);
                objects = list;
            }

        }

        protected IEnumerable<T> GetAll()
        {
            LoadFile();
            return objects;

        }

        protected void Update(int id, T recipe)
        {
            LoadFile();
            objects.RemoveAt(id);
            objects.Insert(id,recipe);
            SaveFile();
        }

        protected void Delete(int id)
        {
            LoadFile();
            objects.RemoveAt(id);
            SaveFile();
        }
    }
}
