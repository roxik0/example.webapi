﻿using System;
using Example.WebApi.Client;

namespace Example.Client
{
    internal class SelectedRecipeMenu:Menu
    {
        private readonly int _selectedRecipe;

        public SelectedRecipeMenu(int selectedRecipe)
        {
            _selectedRecipe = selectedRecipe;

            AddMenuItem("Open Details", OpenRecipe);
            AddMenuItem("Create ShopList", CreateShoplist);
            AddMenuItem("Update Recipe", UpdateRecipe);
            AddMenuItem("Delete Recipe", DeleteRecipe);
            AddMenuItem("Exit", () => ExitMenu=true);
        }

        private void CreateShoplist()
        {
            var ingredients=new WebApiClient().GetIngredients(_selectedRecipe);
            foreach (var ingredient in ingredients)
            {
                Console.WriteLine($"{ingredient.Name} {ingredient.Amount}");    
            }
            ShowPressAnyKeyToContinue();
        }

        private void DeleteRecipe()
        {
            new WebApiClient().DeleteRecipe(_selectedRecipe);
            Console.WriteLine("Deleted");
            ShowPressAnyKeyToContinue();
            ExitMenu = true;
        }

        private void UpdateRecipe()
        {
            var recipe = new WebApiClient().GetRecipe(_selectedRecipe);
            var recipeMenu = new RecipeMenu();
            recipeMenu.Title = $"Update {recipe.Title}";
            recipeMenu.ResultRecipe = recipe;
            recipeMenu.Run();
            Title = $"Update {recipeMenu.ResultRecipe.Title}";
            new WebApiClient().UpdateRecipe(_selectedRecipe,recipeMenu.ResultRecipe);
        }

        private void OpenRecipe()
        {
            var recipe=new WebApiClient().GetRecipe(_selectedRecipe);
            recipe.ShowOnConsole();
            ShowPressAnyKeyToContinue();
        }
    }
}